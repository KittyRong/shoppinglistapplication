'''
comment
'''
from shopping import ShoppingList

def runapp():
    '''
    comment
    '''
    shopping_list = ShoppingList()
    while True:
        operation = input('Select an add, remove, edit, show or clear operation: ')
        if operation == 'add':
                times = input(f'how many items you want to add: ')
                if times.isdigit():
                    times=int(times)
                    for i in range(times):
                        shopping_list.add(input(f'the added item: '))
                        shopping_list.show()
                else:           
                    print('the input should be a numerical value')
        elif operation == 'remove':
            shopping_list.remove(input(f'the removed item: '))
            shopping_list.show()
        elif operation == 'edit':
            shopping_list.edit(input(f'item to be replaced: '),input(f'newly added item: '))
            shopping_list.show()
        elif operation == 'show':
            shopping_list.show()
        elif operation == 'clear':
            shopping_list.clear()
            shopping_list.show()
        else:
            print('operation not found')

runapp()